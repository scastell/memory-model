module Html = Dom_html


let draw_graph g =
  Js.Unsafe.eval_string (Printf.sprintf "drawDot (%S)" g)

let text d v =
  let x = Html.createSpan d in
  x ## innerHTML <- Js.string v; x

let make_link caption action =
  let new_a = Html.createA Html.document in
  new_a ## href <- Js.string "";
  new_a ## onclick <- Html.handler (fun _ -> action (); Js._false);
  Dom.appendChild new_a (text Html.document caption);
  new_a

let checkbox where caption =
  let lbl = Html.createLabel Html.document in
  let inp = Html.createInput ~_type: (Js.string "checkbox") Html.document in
  Dom.appendChild lbl inp;
  Dom.appendChild lbl
    (text Html.document (" " ^ caption));
  Dom.appendChild where lbl;
  Dom.appendChild where (Html.createBr Html.document);
  inp
  
    
let make_example textbox (caption, code) =
  Dom.appendChild
    (Dom_html.getElementById "examples")
    (make_link caption (fun () -> textbox##value <- Js.string code));
  Dom.appendChild
    (Dom_html.getElementById "examples")
    (text Dom_html.document " - ")
    
let init _ =
  let d = Html.document in
  let body = Dom_html.getElementById "main" in
  let textbox = Html.createTextarea d in
  let inputbox_closed = Html.createInput ~_type: (Js.string "submit") d in
  let inputbox_open = Html.createInput ~_type: (Js.string "submit") d in
  let inputbox_outcomes = Html.createInput ~_type: (Js.string "submit") d in
  let checkbox = checkbox (Dom_html.getElementById "settings") in
  let reorder_rr = checkbox "Independent read/read reordering." in
  let reorder_rw = checkbox "Independent read/write reordering." in
  let reorder_ww = checkbox "Independent write/write reordering." in
  let reorder_rrd = checkbox "Dependent read/read reordering." in
  let relaxed = checkbox "Store atomicity relaxing." in
  let bounds = Html.createInput ~_type: (Js.string "text") d in
  let _ = bounds ## value <- Js.string "2" in
  
  let _ =
    reorder_rrd##onclick <- Dom_html.handler (fun _ ->
      if Js.to_bool (reorder_rrd ## checked) then
        reorder_rr ## checked <- Js._true;
      Js._true);
    reorder_rr##onclick <- Dom_html.handler (fun _ ->
      if Js.to_bool (reorder_rrd ## checked) then
        reorder_rrd ## checked <- Js._false;
      Js._true)
  in
  let options () = {
    Model.reorder_rr = Js.to_bool (reorder_rr ## checked);
    Model.reorder_wr = Js.to_bool (reorder_rw ## checked);
    Model.reorder_ww = Js.to_bool (reorder_ww ## checked);
    Model.reorder_rrd = Js.to_bool (reorder_rrd ## checked);
    relaxed = Js.to_bool (relaxed ## checked);
    bounds = int_of_string @@ Js.to_string (bounds ## value);
  }
  in
  let get_program f =
    let code = Js.to_string (textbox ## value) in
    try
      f (Syntax.parse_string "<stdin>" code)
    with Failure s ->
      (Html.getElementById "result") ## innerHTML <- Js.string s
  in
  let semantics prog =
    let opts = options () in
    Model.es_of opts prog
    |> Model.(pullback (if opts.relaxed then valid_trace_relaxed else valid_trace_strict))
  in
  let do_closed _ =
    get_program (fun prog ->
      draw_graph (ES.dot_of Model.string_of_label (semantics prog)));
    Js._false
  in
  let do_open _ =
    get_program (fun prog ->
      draw_graph (ES.dot_of Model.string_of_label
                  @@ Model.es_of (options ()) prog));
    Js._false

  in
  let do_outcomes _ =
    get_program (fun prog ->
      let s = semantics prog
              |> Configuration.max
              |> List.map Configuration.traces |> List.concat 
              |> List.map Model.state_after_trace
              |> List.map Model.Env.bindings 
              |> Prelude.unique
              |> List.map (fun l -> List.map (fun (a, b) -> Printf.sprintf "%s=%d " a b) l
                              |> String.concat " ")
              |> String.concat "\n"
      in (Html.getElementById "result") ## innerHTML <- Js.string ("Possible outcomes:\n" ^ s));
    Js._false
  in
  
  inputbox_open##value <- Js.string "Open interpretation";
  inputbox_closed##value <- Js.string "Closed interpretation";
  inputbox_outcomes##value <- Js.string "Outcomes";
  textbox##rows <- 10; textbox##cols <- 50;
  textbox##value <- Js.string "r <- a; b := r.";
  inputbox_closed##onclick <- Dom_html.handler do_closed;
  inputbox_open##onclick <- Dom_html.handler do_open;
  inputbox_outcomes##onclick <- Dom_html.handler do_outcomes;
  Dom.appendChild body textbox;
  Dom.appendChild body (text d "<br />");
  Dom.appendChild body (text d "Size of ℕ: ");
  Dom.appendChild body bounds;
  Dom.appendChild body (text d "<br />");
  Dom.appendChild body inputbox_open;
  Dom.appendChild body (text d " ");
  Dom.appendChild body inputbox_closed;
  Dom.appendChild body (text d " ");
  Dom.appendChild body inputbox_outcomes;
  List.iter (make_example textbox)
    ["Sequential program.",
     "x := 1; r <- x; x := r + 1";
     "Simple race.",
     "x := 1 || x := 2 || r <- x";
     "Commuting read/writes.",
     "r <- y; x := 1 || r' <- x; y := 1 ";
     "Store buffering",
     "x := 1; r <- x; r' <- y
|| y := 1; s <- y; s' <- x"
    ];
  
  Js._false

let () = 
  Dom_html.window##onload <- Dom_html.handler init
