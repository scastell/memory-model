type t = Ghost
	 | Real of location
and location = {
  filename: string;
  line: int;
  column: int;
}

let loc_of_lexing l =
  let open Lexing in
  Real {filename = l.pos_fname; 
	line = l.pos_lnum;
	column = l.pos_cnum - l.pos_bol}

let str_of_loc (l, l') = match (l, l') with
  | Real {filename; line; column}, 
    Real {filename=filename'; line=line'; column=column'} ->
    filename ^": "^
      (if l = l' then
	  Printf.sprintf "at line %d, column %d" line column
       else if line = line' then
	 Printf.sprintf "at line %d, columns %d-%d" line column column'
       else 
	 Printf.sprintf "from [line %d, column %d] to line [%d, column %d]"
	   line column line' column')
  | _ -> "<ghost>"

let ghost = Ghost, Ghost

let at x lines = match x with
  | Real l, Real m ->
    let print_sub s start stop = 
      let stop = if stop = -1 then String.length s - 1 else stop in
      print_endline (String.sub s start (stop - start))
    in
    let nth k = List.nth lines (k-1) in
    if l.line = m.line then (
      print_string "  ";
      print_sub (nth l.line) l.column m.column
    ) else (
      print_string "  ";
      print_sub (nth l.column) l.column (-1);
      for k = l.column + 1 to m.column-1 do
	Printf.printf "  %s" (nth k)
      done;
      print_string "  ";
      print_sub (nth m.line) 0 m.column
    )
  | _ -> print_endline "<ghost>"
