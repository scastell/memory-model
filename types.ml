type var = string
type reg = string
type arith_expr =
  | Value of reg
  | Add of arith_expr * arith_expr
  | Mult of arith_expr * arith_expr
  | Int of int

let rec freevars = function
  | Value reg -> [reg]
  | Add (e1, e2) | Mult (e1, e2) -> freevars e1 @ freevars e2
  | Int _ -> []

type instr =
  | Let of bool * var * reg
  | Assign of bool * var * arith_expr
  | Ifeq of arith_expr * arith_expr * thread * thread

and thread = instr list

type t = thread list
