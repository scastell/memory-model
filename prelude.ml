let ( |- ) f g = fun x -> g (f x)
let ( -| ) f g = g |- f
let ( ||> ) x f = ignore (f x); x
let flip f x y = f y x

let unique liste =
  let rec aux = function
    | t :: ((t' :: q) as l) when t = t' -> aux l
    | [t] as l -> l
    | t :: q -> t :: aux q
    | [] -> []
  in aux (List.sort compare liste)

let rec filter_map f = function
  | [] -> []
  | t :: q -> match f t with
    | None -> filter_map f q
    | Some x -> x :: filter_map f q

let push_back array v =
  Array.init (Array.length array + 1)
    (fun k -> if k = Array.length array then v else array.(k))


module A = struct
  include Array
  let filter_map f a =
    let r = ref [] in
    Array.iter (fun a -> match f a with
    | Some x -> r := x :: !r
    | None -> ()) a
  let mem x a =
    try
      Array.iter (fun z -> if z = x then failwith "" else ()) a;
      false
    with _ -> true
        
    
end
      
let rec nproduct f = function
  | [] -> [[]]
  | t :: q ->
     let l = nproduct f q in
     List.map (fun i -> List.map (fun j -> (t, i) :: j) l) (f t) |> List.concat

let last l = List.hd (List.rev l)
let rec is_prefix l l' = match (l, l') with
  | [], _ -> true
  | t :: q, t' :: q' when t = t' -> is_prefix q q'
  | _ -> false
let rec is_prefix_strict l l' = match (l, l') with
  | [], [x] -> true
  | t :: q, t' :: q' when t = t' -> is_prefix q q'
  | _ -> false
