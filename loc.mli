(** Location management *)

type t
(** Location represent a point in a source code.
    We only handle couple of location that represent a 
    range in the file *)

val str_of_loc : (t * t) -> string
(** Describe the range represented by the argument to a string. *)

val at : (t * t) -> string list -> unit
(** Print the source code in a range given the lines of the file. *)

val loc_of_lexing : Lexing.position -> t
(** Get a location from the lexer's lexbuf *)


val ghost : t * t
(** A ghost location that does not represent anything *)
