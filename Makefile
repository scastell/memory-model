OCAMLBUILD=ocamlbuild -use-menhir -use-ocamlfind


all: web.js model

model: main.native
	mv $< $@

web.js: web.byte
	js_of_ocaml $<

web.byte:
	$(OCAMLBUILD) $@
test.native:
	$(OCAMLBUILD) $@

main.native:
	$(OCAMLBUILD) $@

main.cma:
	$(OCAMLBUILD) $@

upload:
	scp *css *js demo.html index.html phis.me:public_html/memory

.PHONY: web.js web.byte main.native model
