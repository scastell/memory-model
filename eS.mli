type 'a t = {
  labels: 'a array;
  causality: (int * int) list;
  conflicts: (int * int) list;
}

val single: 'a -> 'a t
val empty: 'a t
  
val successors : 'a t -> int -> int list
val predecessors : 'a t -> int -> int list

val is_conflict: 'a t -> int -> int -> bool
  
val events: 'a t -> int list
val min : 'a t -> int list

val dot_of : ?not_web: bool -> ('a -> string) -> 'a t -> string

val min_such_that : (int -> 'a -> bool) -> 'a t -> int list
  
(* Constructions *)
val prefix : 'a -> 'a t -> 'a t
val parallel : 'a t list -> 'a t
val sum : 'a t list -> 'a t

val from_fun: ('a -> 'b) -> ('a -> 'a -> bool) -> ('a -> 'a -> bool) -> 'a list -> 'b t

val propagate_conflict : 'a t -> 'a t
val minimal_causality : 'a t -> 'a t  
val concatenate: 'a t -> 'a t -> ('a -> 'a -> bool) -> 'a t
