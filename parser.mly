%{
  open Types
%}

/* Syntaxic tokens */
%left ADD
%left MULT
%token ADD MULT PARALLEL LEFTARROW SEMICOLON LEFTBANGARROW ASSIGN BANGASSIGN EQUAL EOF IF LPAR RPAR LBRACE RBRACE
%token<string> IDENT
%token<int> INT

%type <Types.t> program
%start program
%% 

expr:
| INT { Int $1 }
| IDENT { Value $1 }
| expr ADD expr { Add ($1, $3) }
| expr MULT expr { Mult ($1, $3) }
| LPAR expr RPAR { $2 }

instr:
| IDENT LEFTARROW IDENT { Let (false, $1, $3) }
| IDENT LEFTBANGARROW IDENT { Let (true, $1, $3) }
| IDENT ASSIGN expr { Assign (false, $1, $3) }
| IDENT BANGASSIGN expr { Assign (true, $1, $3) }
| IF LPAR expr EQUAL expr RPAR LBRACE thread RBRACE LBRACE thread RBRACE
    { Ifeq ($3, $5, $8, $11) }
| IF LPAR expr EQUAL expr RPAR LBRACE thread RBRACE
    { Ifeq ($3, $5, $8, []) }
thread:
| instr { [$1] }
| instr SEMICOLON { [$1] }
| instr SEMICOLON thread { $1 :: $3 }
program:
| thread EOF { [$1] }
| thread PARALLEL program { [$1] }
