{ 
open Parser
}
rule token = parse
  | ['\n'] { Lexing.new_line lexbuf; token lexbuf }
  | [' ' '\t']+ { token lexbuf }
  | "<-" | "←" { LEFTARROW  }
  | "<!-" { LEFTBANGARROW  }
  | "=" { EQUAL }
  | ":=" { ASSIGN }
  | ":!=" { BANGASSIGN }
  | "||" | "∥" { PARALLEL  }
  | "+" { ADD  }
  | "(" { LPAR  }
  | ")" { RPAR  }
  | "{" { LBRACE  }
  | "}" { RBRACE }
  | "if" { IF  }
  | "*" { MULT  }
  | "||" | "∥" { PARALLEL  }
  | ";" | "." { SEMICOLON  }
  | ['a'-'z' '\'' '_' 'A'-'Z']['a'-'z' '\'' '_' 'A'-'Z' '0'-'9']* as s { IDENT (s) }
  | ['0'-'9']+ as s { INT (int_of_string s) }
  | eof { EOF }
