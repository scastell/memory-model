open Prelude

let bounds = ref 10
let reorder_rr = ref true
let reorder_wr = ref true
let reorder_ww = ref true
let reorder_rrd = ref false
let relaxed_memory = ref false
let filename = ref "-"
let output = ref "-"
let string = ref ""
let mode = ref (`Closed : [ `Closed | `Open | `Outcomes])
  
let _ =
  Arg.(parse
         (align
            [
              "--sc", Unit (fun () ->
                reorder_rr := false; reorder_wr := false;
                reorder_wr := false),
              " Forbid any instruction reordering (default: no)";
              "--bounds", Set_int bounds, "<bounds> Size of the integers (Default: 10)";
              "--no-load/load", Clear reorder_rr, " Disllow reordering of independent load/load. (Default: yes)";
              "--no-load/store", Clear reorder_wr, " Disllow reordering of independent load/store. (Default: yes)";
              "--no-store/store", Clear reorder_ww, " Disllow reordering of independent store/store. (Default: yes)";
              "--dep-load/load", Set reorder_rrd, " Allow reordering of *dependent* load/load. (Default: no)";
              "--relaxed-memory", Set relaxed_memory, " Use a relaxed memory semantics. (Default: no).";
              "--file", Set_string filename, "<filename> Input file.";
              "--output", Set_string output, "<filename> Output file.";
              "--outcomes", Unit (fun () -> mode := `Outcomes), " Compute the possible outcomes.";
              "--open", Unit (fun () -> mode := `Open), " Only compute the open interpretation (outputs dot).";
            ])
         (fun s -> string := s)
         "An implementation of a memory model using event structures.
The default usage is: ./model 'Source code'.
This outputs dot code describing the event structures corresponding to the source code.
See the options for more advanced usage.")

let opts () = {
  Model.reorder_rr = !reorder_rr;
  reorder_rrd = !reorder_rrd;
  reorder_wr = !reorder_wr;
  reorder_ww = !reorder_ww;
  relaxed = !relaxed_memory;
  bounds = !bounds;
}

let source_code () =
  if !string <> "" then
    Syntax.parse_string "<user string>" !string
  else if !filename = "-" then
    Syntax.parse_channel "<stdin>" stdin
  else
    let fd = open_in !filename in
    let p = Syntax.parse_channel !filename fd in
    close_in fd; p


let exec program fd =
  let opts = opts () in
  let pure = Model.es_of opts program in
  let output e =
    Printf.fprintf fd "%s\n" (ES.dot_of ~not_web: true Model.string_of_label e)
  in
  let closed () =
    Model.pullback
      (if opts.Model.relaxed then Model.valid_trace_relaxed
       else Model.valid_trace_strict)
      pure
  in
  match !mode with
  | `Open -> output pure
  | `Closed -> output (closed ())
  | `Outcomes ->
     let closed = closed () in
     let open List in
     closed |> Configuration.max |> map Configuration.traces
  |> map hd |> map (Model.state_after_trace |- Model.Env.bindings)
  |> Prelude.unique |> iter (fun l ->
    iter (fun (a, b) -> Printf.fprintf fd "%s=%d " a b) l;
    print_endline "")
       
       
let main =
  if not !Sys.interactive then
    exec (source_code ())
      (if !output = "-" then stdout else open_out !output)
