include Types
open Parser

let string_of_token =
  let spr = Printf.sprintf in
  function
  | ADD -> "<ADD(+)>" | MULT -> "<MULT(*)>" | PARALLEL -> "<PARALLEL(||)>"
  | LEFTARROW -> "<LEFTARROW(<-)>" | SEMICOLON -> "<SEMICOLON(;)>"
  | LEFTBANGARROW -> "<LEFTBANGARROW(<!-)>" | ASSIGN -> ":=" | EQUAL -> "="
  | BANGASSIGN -> ":!="
  | IDENT s -> spr "%S" s
  | INT k -> spr "INT(%d)" k
  | EOF -> "<eof>"
  | LPAR -> "(" | RPAR -> ")" | LBRACE -> "{" | RBRACE -> "}" | IF -> "if"
     
let token () = 
  let last_token = ref (IDENT "<beginning>") in
  let last_pos = ref (fst Loc.ghost) in
  let curr_pos = ref (!last_pos) in
  (fun lexbuf ->
    let tok = Lexer.token lexbuf in
    last_token := tok;
    last_pos := Loc.loc_of_lexing lexbuf.Lexing.lex_start_p;
    curr_pos := Loc.loc_of_lexing lexbuf.Lexing.lex_curr_p;
    tok),
  (fun () -> !last_token, !last_pos, !curr_pos)

(* Parses from a lexbuf, catching parse error *)
let parse_lex lexbuf = 
  let token, fetch = token () in
  try Parser.program token lexbuf
  with Parser.Error ->
    let tok, p1, p2 = fetch () in
    failwith (Printf.sprintf
      "[%s] Parse error: On token %s"
      (Loc.str_of_loc (p1, p2)) (string_of_token tok))
    | Failure "lexing: empty token" ->
      let tok, p1, p2 = fetch () in
      failwith (Printf.sprintf "[%s] Lexing error: unexpected token %s"
	(Loc.str_of_loc (p1, p2)) (string_of_token tok))

let set_fname fname lexbuf = 
  lexbuf.Lexing.lex_curr_p <- { lexbuf.Lexing.lex_curr_p with
    Lexing.pos_fname = fname
  };
  lexbuf
let parse_string fname s = parse_lex (set_fname fname (Lexing.from_string s))
let parse_channel fname fd = parse_lex (set_fname fname (Lexing.from_channel fd))
