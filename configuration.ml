open Prelude
module S_ = Set.Make (struct type t = int let compare = compare end)
module S = struct
  include S_
  let of_list l =
    List.fold_left (flip S_.add) S_.empty l
end
type 'a t = 'a ES.t * S.t

let is_an_extension (es, conf) e =
  not (S.mem e conf)
  && List.for_all (flip S.mem conf) (ES.predecessors es e)
  && S.for_all (not -| ES.is_conflict es e) conf

let extensions (es, conf) = ES.events es |> List.filter (is_an_extension (es, conf))
let list es =
  let confs = ref [] in
  let seen c = List.exists (fun (_, c') -> S.equal c c') !confs in
  let rec aux conf =
    let rest = ES.events es |> List.filter (is_an_extension (es, conf)) in
    if not (seen conf) then begin
      confs := (es, conf) :: !confs;
      List.iter (fun ext ->
          let conf' = S.add ext conf in
          aux conf') rest
    end
  in List.iter (fun i -> aux (S.singleton i)) (ES.min es);
  !confs

let max es =
  let confs = ref [] in
  let rec aux conf =
    let is_an_extension e =
      not (S.mem e conf)
      && List.for_all (flip S.mem conf) (ES.predecessors es e)
      && S.for_all (not -| ES.is_conflict es e) conf
    in
    let rest = ES.events es |> List.filter is_an_extension in
    if rest = [] then confs := (es, conf) :: !confs;
    List.iter (fun ext -> aux (S.add ext conf)) rest;
  in
  List.iter (fun i -> aux (S.singleton i)) (ES.min es);
  unique !confs
  
let maximal_events (es, set) =
  flip S.filter set (fun e ->
    List.for_all (fun k -> not (S.mem k set))
      (ES.successors es e))

let hide f (es, conf) = (es, S.filter f conf)
let rec traces_aux (es, conf) =
  if S.is_empty conf then [[]]
  else
    maximal_events (es, conf)
            |> S.elements
            |> List.map
                (fun m ->
                  hide ((<>) m) (es, conf)
                    |> traces_aux
                    |> List.map (fun l -> m :: l))
            |> List.concat

let traces (es, conf) = List.map (fun l -> es, List.rev l) (traces_aux (es, conf))

let to_string show (es, conf) =
  String.concat ", " (List.map (fun k -> show es.ES.labels.(k)) (S.elements conf))

let trace_to_string show (es, tr) =
  String.concat "→" (List.map (fun k -> show es.ES.labels.(k)) tr)
