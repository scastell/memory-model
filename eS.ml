open Prelude

type 'a t = {
  labels: 'a array;
  causality: (int * int) list;
  conflicts: (int * int) list;
}

let single a = {
  labels = Array.make 1 a;
  conflicts = [];
  causality = []
}

let successors { causality } e =
  filter_map (fun x -> if fst x = e then Some (snd x) else None) causality

let predecessors { causality } e =
  filter_map (fun x -> if snd x = e then Some (fst x) else None) causality

let is_conflict es e e' = List.mem (e, e') es.conflicts

let events es =
  Array.(to_list (init (length es.labels) (fun i -> i)))
let empty = { causality = []; conflicts = []; labels = [||] }

let min { labels; causality } =
  let a = Array.init (Array.length labels) (fun k -> false) in
  let results = ref [] in
  List.iter (fun (_, x) -> a.(x) <- true) causality;
  Array.iteri (fun i b -> if not b then results := i :: !results) a;
  !results 

let rec below es s s' =
  s = s' || List.exists (flip (below es) s') (successors es s)

let min_such_that pred es =
  let found = ref [] in
  let rec aux = function
    | e :: q when pred e es.labels.(e) ->
      if not @@ List.exists (flip (below es) e) !found then
        found := e :: !found;
      aux q
    | e :: q ->
      aux (q @ successors es e)
    | [] -> ()
  in aux (min es); !found

let below_strict es s s' =
  List.exists (flip (below es) s') (successors es s)
let minimal_conflicts es =
  let result = ref [] in
  List.iter (fun (i, j) ->
      if not @@ List.exists (fun (k, k') -> below es k i && below es k' j && (k, k') <> (i, j)) es.conflicts
      && not @@ List.mem (i, j) !result then
        result := (i, j) :: !result;
    ) es.conflicts; !result

let minimal_causality es =
  let transitive = es.causality in
  let immediate = List.filter (fun (i, j) ->
      i <> j
      && not (flip List.exists (events es)
                (fun z -> i <> z && z <> j &&
                          List.mem (i, z) transitive && List.mem (z, j) transitive)))
      transitive
  in
  { es with causality = immediate }

let dot_of ?(not_web = false) show es =
  let spr = Printf.sprintf in
  let event_name = spr "Var%d" in
  let box i label =
    spr
      (if not_web then "  %s [labelType=\"html\" label=%S];\n"
       else " %s [label=%S];\n")
      (event_name i) (show es.labels.(i))
  and causality (i, j) =
    spr " %s -> %s %s;\n" (event_name i) (event_name j)
      (if not_web then "" else "[class=\"causality\"]")
  and conflict (i, j) = if i <= j then "" else
      spr " %s -> %s %s;\n" (event_name i) (event_name j)
        (if not_web then "[color=red dir=none]"
         else "[class=\"conflict\" arrowheadStyle=\"stroke: none; fill: none\"]")
  in
  let map_concat map f sep l = String.concat sep (map f l) in
  spr " digraph es { \n%s\n%s\n%s }"
    (map_concat (fun f l -> Array.to_list (Array.mapi f l)) box "" es.labels)
    (map_concat List.map causality "" es.causality)
    (map_concat List.map conflict "" (minimal_conflicts es))


let prefix label es = 
  { es with
    labels = push_back es.labels label;
    causality = List.map (fun j -> Array.length es.labels, j) (min es)
                @ es.causality
  }

let product l l' =
  List.concat (List.map (fun i -> List.map (fun j -> (i, j)) l') l)
let symm_product l l' = product l l' @ product l' l

let propagate_conflict es =
  let conflicts = ref es.conflicts in
  let rec iterate (i, j) =
    if not (List.mem (i, j) !conflicts) then
      conflicts := (i, j) :: !conflicts;
    flip List.iter (i :: successors es i) (fun s1 ->
        flip List.iter (j :: successors es j) (fun s2 ->
            if (s1, s2) <> (i, j) then iterate (s1, s2)))
  in
  List.iter iterate es.conflicts;
  { es with conflicts = !conflicts }



let parallel ?(conflict = false) ess =
  let shift k l = List.map (fun (i, j) -> (k+i, k+j)) l in
  let (causality, conflicts, mins, _) =
    List.fold_left (fun (causality, conflicts, mins, k) es ->
        let causality = shift k es.causality @ causality
        and conflicts = shift k es.conflicts @ conflicts
        and mins = List.map ((+)k) (min es) :: mins in
        (causality, conflicts, mins, k + Array.length es.labels)
      ) ([], [], [], 0) ess
  in
  let extra_conflicts, _ =
    if conflict then
      List.fold_left
        (fun (conflicts, to_be_conflicted_with) l ->
           (symm_product l to_be_conflicted_with @ conflicts,
            l @ to_be_conflicted_with)) ([], []) mins
    else [], []
  in
  propagate_conflict
    { labels = Array.concat (List.map (fun {labels} -> labels) ess);
      causality; conflicts = extra_conflicts @ conflicts}
let sum l = parallel ~conflict: true l
let parallel l = parallel l
let from_fun label causality conflicts list =
  let events = Array.of_list list in
  let square = List.mapi (fun i _ -> List.mapi (fun j _ -> (i, j)) list) list
               |> List.concat
  in
  let filter f = List.filter (fun (i, j) -> f events.(i) events.(j)) square in
  minimal_causality
    { labels = Array.of_list (List.map label list); 
      causality = filter causality; conflicts = filter conflicts
    }  

let concatenate es1 es2 causality =
  let es = parallel [es1; es2] in
  let (n1, n2) = Array.(length es1.labels, length es2.labels) in
  let extra = ref es.causality in
  for i = 0 to n1 - 1 do
    for j = 0 to n2 - 1 do
      if causality es1.labels.(i) es2.labels.(j) then
        extra := (i, j+n1) :: !extra
    done
  done;
  minimal_causality { es with causality = !extra }
      
