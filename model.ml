open Prelude
open Syntax
module S = Configuration.S
let spr = Printf.sprintf
type var = string
type reg = string

type env = (string * (int option * string)) list

and label = {
  var: var;
  value: int;
  kind: [`Read of reg | `Write];
  tid: int;
  reg_env: (reg * int) list;
}

let string_of_env env =
  let m = function
    | (v, (Some k, r)) -> Printf.sprintf "%s=%d[alias:%s]" v k r
    | (v, (None, r)) -> Printf.sprintf "%s[alias:%s]" v r
  in List.map m env |> String.concat ", "

let string_of_simple_env env =
  spr "[%s]" (String.concat "," (List.map (fun (a, b) -> spr "%s=%d" a b) env))
    
let string_of_label {reg_env; var; value; tid; kind} = match kind with
  | `Write ->
    Printf.sprintf "w%s=%d [tid %d] %s" var value tid
      (string_of_simple_env reg_env)
  | `Read reg -> Printf.sprintf "r%s=%d [on: %s] [tid %d] %s" var value reg tid
                   (string_of_simple_env reg_env)


type ('a, 'b) _or = Left of 'a | Right of 'b

let assoc v env =
  try List.assoc v env
  with Not_found ->
    failwith (Printf.sprintf "Register variable (let-bound) `%s' not found." v)
let rec eval env = function
  | Add (a, b) ->
    let l = eval env a and l' =  eval env b in
    l + l'
  | Mult (a, b) ->
    let l = eval env a and l' =  eval env b in
    l * l'
  | Int k -> k
  | Value r -> assoc r env


type reorder = {
  reorder_ww: bool;
  reorder_wr: bool;
  reorder_rr: bool;
  reorder_rrd: bool; (* reorder dependent reads *)
  relaxed: bool;
  bounds: int;
}

let rec es_of_thread force_env force_var opts tid env =
  let bounds = Array.to_list (Array.init opts.bounds (fun i -> i)) in
  let rec process env es = function
    | [] -> es env
    | v :: q when not (List.mem_assoc v force_env) ->
       ES.parallel @@ flip List.map bounds (fun k ->
         process ((v, k) :: env) es q)
    | _ :: q -> process env es q
  in
  let process = process force_env in
  let regs_of_var env var = flip filter_map env
      (fun (reg, var') ->
         if var = var' then Some reg
         else None)
  in
  let env_compatible env1 env2 = List.for_all (flip List.mem env2) env1 in
  let is_dep e e' =
    let simple = match (e.kind, e'.kind) with
    | `Write, `Write -> not opts.reorder_ww
    | `Read _, `Write | `Write, `Read _ -> not opts.reorder_wr
    | `Read _, `Read _ ->
       if e.var = e'.var then not opts.reorder_rrd else not opts.reorder_rr
    in
    (env_compatible e.reg_env e'.reg_env)
    && (simple || (match e.kind with
    | `Write -> e.var = e'.var
    | `Read r -> List.mem_assoc r e'.reg_env))
  in
  function
  | Assign (sequential, var, expr) :: thr ->
     let regs = unique (freevars expr @ force_var `Wr) in
     let t = es_of_thread force_env force_var opts tid env thr in
     let event reg_env =
       { kind = `Write; tid; value = eval reg_env expr; reg_env; var }
     in
     if sequential then
       process (fun reg_env -> ES.prefix (event reg_env) t) regs
     else
       ES.concatenate (process (event |- ES.single) regs) t is_dep

  | Let (sequential, name, var) :: thr ->
     let regs = unique @@ force_var `Re in
     let force_var kind =
       let l = force_var kind in
       match kind with
       | `Re -> if sequential || not opts.reorder_rr then (name :: l) else l
       | `Wr -> if sequential || not opts.reorder_wr then (name :: l) else l
     in
     if not sequential then
       let t = es_of_thread force_env force_var opts tid ((name, var) :: env) thr in
       let generate reg_env =
         let event k = { kind = `Read name; tid; value = k; var;
                         reg_env = (name, k) :: reg_env } in
           ES.sum @@ List.map (ES.single -| event) bounds
       in
       ES.concatenate (process generate regs) t is_dep
     else
       ES.sum @@ flip List.map bounds (fun k ->
         let generate reg_env = 
           let event k = { kind = `Read name; tid; value = k; var;
                           reg_env = (name, k) :: reg_env } in
           let make k =
             ES.prefix (event k)
               (es_of_thread ((name, k) :: force_env) force_var opts tid ((name, var) :: env) thr)
           in 
           ES.sum @@ List.map make bounds
         in process generate regs)
       
  | Ifeq (e1, e2, yes, no) :: thr ->
     let regs = unique (freevars e1 @ freevars e2) in
     let generate reg_env =
       if eval reg_env e1 = eval reg_env e2 then
         es_of_thread reg_env force_var opts tid env (yes @ thr)
       else
         es_of_thread reg_env force_var opts tid env (no @ thr)
     in
     process generate regs
  | _ ->
    ES.empty

let es_of opts p = ES.parallel (List.mapi (fun i -> es_of_thread [] (fun _ -> []) opts i []) p)


let valid_trace_strict (es, t) =
  let rec aux env = function
    | [] -> true
    | t :: q ->
      let event = es.ES.labels.(t) in
      match event.kind with
      | `Read _ ->
        (try List.assoc event.var env with _ -> 0) = event.value && aux env q
      | `Write  ->
        aux ((event.var, event.value) :: env) q
  in aux [] t

let valid_trace_relaxed (es, t) = 
  let t = List.map (fun e -> es.ES.labels.(e)) t in
  let assoc x v = try List.assoc x v with _ -> 0 in
  let update tid v env = (tid, v) :: List.remove_assoc tid env in
  let rec aux tenv env = function
    | [] -> true
    | event :: q ->
      match event.kind with
      | `Write  -> aux (update (event.var, event.tid) event.value tenv)
         (update event.var event.value env) q
      | `Read _ ->
        let local_value = assoc (event.var, event.tid) tenv in
        if event.value = local_value then
          aux tenv env q
        else if event.value = assoc event.var env then
          aux (update (event.var, event.tid) event.value tenv) env q
        else false
  in aux [] [] t

let names = ["x"; "y"; "z"]
let v = ref (-1)
let prime_configurations valid_traces ((es, set) as conf) =
  let var_of k = es.ES.labels.(k).var in
  let names = unique @@ List.map var_of @@ S.elements set in
  let traces = Configuration.traces conf |> List.filter valid_traces in
  let filter vname =
    fun lbl -> es.ES.labels.(lbl).var = vname
  in
  let groups vname =
    List.map (fun (es, tr) -> es, List.filter (filter vname) tr) traces
    |> unique |> List.filter valid_traces
  in
  let possibilities = nproduct groups names in
  let events = flip filter_map possibilities (fun possibility ->
      let maxs = List.map (fun (name, (_, l)) -> (name, last l)) possibility in
      let m = List.filter (fun (name, s) ->
          List.for_all (not -| flip S.mem set) (ES.successors es s)) maxs
      in
      match m with
      | [(var, e)] -> Some (conf, var, e, names, possibility)
      | _ -> None) |> unique
  in
  events

let pullback valid_traces es =
  let ccs = Configuration.list es
            |> List.map (prime_configurations valid_traces)
            |> List.concat |> unique
  in
(*  let _ =   List.iter (fun (configuration, var, top, names, traces) ->
      incr v;
      Printf.printf "Configuration %d [%s]: {%s}, names: [%s]\n  %s\n" !v
        (string_of_label es.ES.labels.(top))
        (Configuration.to_string string_of_label configuration)
        (String.concat ", " names)
        (String.concat "\n  "
           (List.map (fun (var, (_, tr)) -> Printf.sprintf "%s: %s" var
        (String.concat "→" (List.map (fun k -> string_of_label es.ES.labels.(k)) tr))) traces)))      ccs
    in*)
  let assoc n groups = try snd (List.assoc n groups) with Not_found -> [] in
  ES.from_fun
    (fun (_, _, top, _, _) -> es.ES.labels.(top))
    (fun ((_, set1), _, top1, names1, groups1) ((_, set2), _, top2, names2, groups2) ->
       Configuration.S.subset set1 set2 &&
       List.for_all (fun n -> is_prefix (assoc n groups1) (assoc n groups2))
         (unique (names1 @ names2))
    )
    (fun (_, _, top1, names1, groups1) (_, _, top2, names2, groups2) ->
       let names = unique (names1@names2) in
       let uncomparable x y = not (is_prefix x y || is_prefix y x) in
       List.mem (top1, top2) es.ES.conflicts
       || List.exists (fun name -> uncomparable (assoc name groups1) (assoc name groups2)) names;
    ) ccs


module Env = Map.Make (struct
    type t = string
    let compare= compare
  end)

let state_after_trace (es, t) =
  let env = Env.empty in
  let rec walk env = function
    | [] -> env
    | t :: q ->
      let event = es.ES.labels.(t) in
      match event.kind with
      | `Read reg ->
        walk (Env.add reg event.value (Env.remove reg env)) q
      | `Write  ->
        walk (Env.add event.var event.value (Env.remove event.var env)) q
  in
  walk env t


